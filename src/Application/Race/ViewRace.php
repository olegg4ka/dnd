<?php


namespace App\Application\Race;

/**
 * Расы
 * Class ViewRace
 * @package App\Application\Race
 */
class ViewRace
{
	/**
	 * @param $db_info
	 * @return array[]
	 */
	public function getInfoRace($db_info): array
	{
		$info = [];
		foreach ($db_info as $key => $elem) {
			$info[$key] = [
				'name' => $elem->getName()
			];
		}
		return [
			'race' => $info
		];
	}
}