<?php


namespace App\Application\Core\MonologLogger;
use Monolog\Handler\FirePHPHandler;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Psr\Log\LoggerInterface;

/**
 * Class MonologLogger
 * @package App\Application\Core\MonologLogger
 */
class MonologLogger
{
	/**
	 * @param $msg
	 */
	public function db_log($msg)
	{
		$logger = new Logger('db');
		$logger->pushHandler(new StreamHandler(ROOT_PATH.'data/log/db.log', Logger::DEBUG));
		$logger->pushHandler(new FirePHPHandler());
		$logger->info('TEST');
//		$logger->error('An error occurred');
//		$logger->critical('I left the oven on!', array(
//			'cause' => 'in_hurry',
//		));
	}
}