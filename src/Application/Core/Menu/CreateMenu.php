<?php


namespace App\Application\Core\Menu;

/**
 * Меню
 * Class CreateMenu
 * @package App\Application\Core\Menu
 */
class CreateMenu
{
	/**
	 * Метод сбора менюшки
	 * @param $data
	 * @return array[]
	 */
	public function renderMenu($data):array
	{
		$menu = [];
		$subMenu = [];
		foreach ($data as $key => $elem) {
			if ($elem->getHierarchy() == 1) {
				$menu[$key] = [
					'title' => $elem->getTitle(),
					'hid' => $elem->getHierarchy(),
					'code' => $elem->getHierarchycode(),
					'link' => $elem->getRoutelink()
				];
			} else {
				$subMenu[$key] = [
					'title' => $elem->getTitle(),
					'hid' => $elem->getHierarchy(),
					'code' => $elem->getHierarchycode(),
					'link' => $elem->getRoutelink()
				];
			}
		}
		return $data = [
			'menu' => $menu,
			'submenu' =>$subMenu
		];
	}
}