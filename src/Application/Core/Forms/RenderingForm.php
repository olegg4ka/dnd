<?php


namespace App\Application\Core\Forms;


use App\Controller\Auth\AuthController;
use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RenderingForm extends AbstractType
{

	public function buildForm(FormBuilderInterface $builder, array $options):void
	{
		$builder
			->add('login', TextType::class, [
				'attr' => [
					'placeholder' => 'msg',
					'class' => 'customClass'
				]
			])
			->add('password', PasswordType::class)
			->add('save', SubmitType::class, [
				'attr' => [
					'class' => 'btn btn-success'
				],
				'label' => 'Save'
			]);
	}



}