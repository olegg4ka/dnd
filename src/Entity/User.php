<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 */
class User implements UserInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $login;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $password;

	/**
	 * @ORM\Column(type="json")
	 */
	private $roles = [];

	/**
	 * User constructor.
	 */
	public function __construct()
	{
		$this->roles = array('ROLE_USER');
	}

	/**
	 * @return int|null
	 */
	public function getId(): ?int
    {
        return $this->id;
    }

	/**
	 * @return string|null
	 */
    public function getLogin(): ?string
    {
        return $this->login;
    }

	/**
	 * @param string $login
	 * @return $this
	 */
    public function setLogin(string $login): self
    {
        $this->login = $login;
        return $this;
    }

	/**
	 * @return string|null
	 */
    public function getPassword(): ?string
    {
        return $this->password;
    }

	/**
	 * @param string $password
	 * @return $this
	 */
    public function setPassword(string $password): self
    {
        $this->password = $password;
        return $this;
    }

	/**
	 * A visual identifier that represents this user.
	 *
	 * @see UserInterface
	 */
	public function getUsername(): string
	{
		return (string) $this->login;
	}

	/**
	 * @see UserInterface
	 */
	public function getRoles(): array
	{
		$roles = $this->roles;
		// guarantee every user at least has ROLE_USER
		$roles[] = 'ROLE_USER';
		return array_unique($roles);
	}

	/**
	 *
	 */
	public function setRoles(array $roles): self
	{
		$this->roles = $roles;
		return $this;
	}

	public function isPasswordValid(UserInterface $user, string $raw){

	}



	/**
	 * @see UserInterface
	 */
//	public function getPassword()
//	{
//		// not needed for apps that do not check user passwords
//	}
	/**
	 * @see UserInterface
	 */
	public function getSalt()
	{
		// not needed for apps that do not check user passwords
	}
	/**
	 * @see UserInterface
	 */
	public function eraseCredentials()
	{
		// If you store any temporary, sensitive data on the user, clear it here
		// $this->plainPassword = null;
	}

}
