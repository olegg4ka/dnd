<?php

namespace App\Entity;

use App\Repository\MenuRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=MenuRepository::class)
 */
class Menu
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    private $hierarchycode;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $hierarchy;

	/**
	 * @ORM\Column(type="string", length=255, nullable=true)
	 */
    private $routelink;

    public function getId(): ?int
    {
        return $this->id;
    }

	/**
	 * @return string|null
	 */
	public function getRoutelink(): ?string
	{
		return $this->routelink;
	}

	/**
	 * @param string|null $rourtelink
	 * @return $this
	 */
	public function setRoutelink(?string $routelink): self
	{
		$this->routelink = $routelink;

		return $this;
	}

	public function getHierarchycode(): ?string
	{
		return $this->hierarchycode;
	}

	public function setHierarchycode(?string $hierarchycode): self
	{
		$this->hierarchycode = $hierarchycode;

		return $this;
	}

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getHierarchy(): ?float
    {
        return $this->hierarchy;
    }

    public function setHierarchy(float $hierarchy): self
    {
        $this->hierarchy = $hierarchy;

        return $this;
    }
}
