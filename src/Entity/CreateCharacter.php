<?php

namespace App\Entity;

use App\Repository\CreateCharacterRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CreateCharacterRepository::class)
 */
class CreateCharacter
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nameplayer;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $namecharacter;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $class;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $race;

    /**
     * @ORM\Column(type="integer")
     */
    private $userid;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNameplayer(): ?string
    {
        return $this->nameplayer;
    }

    public function setNameplayer(?string $nameplayer): self
    {
        $this->nameplayer = $nameplayer;

        return $this;
    }

    public function getNamecharacter(): ?string
    {
        return $this->namecharacter;
    }

    public function setNamecharacter(?string $namecharacter): self
    {
        $this->namecharacter = $namecharacter;

        return $this;
    }

    public function getClass(): ?string
    {
        return $this->class;
    }

    public function setClass(?string $class): self
    {
        $this->class = $class;

        return $this;
    }

    public function getRace(): ?string
    {
        return $this->race;
    }

    public function setRace(?string $race): self
    {
        $this->race = $race;

        return $this;
    }

    public function getUserid(): ?int
    {
        return $this->userid;
    }

    public function setUserid(int $userid): self
    {
        $this->userid = $userid;

        return $this;
    }
}
