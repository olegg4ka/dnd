<?php

namespace App\Repository;

use App\Entity\CreateCharacter;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CreateCharacter|null find($id, $lockMode = null, $lockVersion = null)
 * @method CreateCharacter|null findOneBy(array $criteria, array $orderBy = null)
 * @method CreateCharacter[]    findAll()
 * @method CreateCharacter[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CreateCharacterRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CreateCharacter::class);
    }

    // /**
    //  * @return CreateCharacter[] Returns an array of CreateCharacter objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CreateCharacter
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
