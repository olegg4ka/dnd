<?php

namespace App\Controller\Admin;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * Class AdminController
 * @package App\Controller\Admin
 *
 * Require ROLE_ADMIN for *every* controller method in this class.
 * @IsGranted("ROLE_ADMIN")
 */
class AdminController extends AbstractController
{
	/**
	 * @return Response
	 * Require ROLE_ADMIN for only this controller method.
	 * @IsGranted("ROLE_ADMIN")
	 */
	public function adminDashboard(): Response
	{
		$this->denyAccessUnlessGranted('ROLE_ADMIN');
		$this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'User tried to access a page without having ROLE_ADMIN');

	}

}