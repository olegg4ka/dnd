<?php

namespace App\Controller\Admin\Prints\PDF;


use App\Configs\PrintsConfigs;
use App\Entity\CreateCharacter;
use mikehaertl\wkhtmlto\Pdf;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * Class PrintsController
 * @package App\Controller\Admin\Prints\PDF
 */
class PrintsController extends AbstractController
{
	/**
	 * @Route("/print", name="print")
	 * @param $data
	 */
	public function printpdf(Request $request):Response
	{
		$pdf = $this->getCreatePdf();
		if ($pdf){
			$pdf->send($pdf->getPdfFilename());
		}
		return $this->render(
			'core/prints/print.html.twig');
	}

	/**
	 * Получаем твиг файл с даными для ПДФ
	 * @return string
	 */
	private function createTwigForPdf(): string
	{
		$id = $this->getUser()->getId();
		$connection = $this->getDoctrine()->getRepository(CreateCharacter::class);
		$data = $connection->findBy(['userid'=>$id], null, null,null);
		$pdf = $this->render('/core/prints/test.html.twig', [
			'data'=> [
				'nameplayer' => end($data)->getNameplayer(),
				'namecharacter' => end($data)->getNamecharacter(),
				'race' => end($data)->getRace(),
				'class' => end($data)->getClass(),
			]
		]);
		return $pdf;
	}

	/**
	 * Создаем ПДФ
	 * @return Pdf
	 */
	private function getCreatePdf(): Pdf
	{
		$pdf = new Pdf();
		$pdf->binary = (PrintsConfigs::wkhtmltopdf_path);
		$pdf->setOptions(PrintsConfigs::defaultOptions);
		$data = $this->createTwigForPdf();
		$pdf->addPage($data);
		$pdf->saveAs($this->generateNameForPDF());
		return $pdf;
	}

	/**
	 * Получаем хтмл
	 * @return string
	 */
	private function getFileData(): string
	{
		/**
		 * TODO пока что бдует указано прямой путь к печати твига
		 */
		return file_get_contents(PrintsConfigs::print_twig_path);
	}

	/**
	 * Генерации имени для пдф
	 * @return string
	 */
	private function generateNameForPDF():string
	{
		return ROOT_PATH . 'data/CharacterProfile/CharacterProfile'.$this->getUser()->getId().'_'.(string)rand().'.pdf';
	}

}