<?php

namespace App\Controller\Admin\Git;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class GitController
 * @package App\Controller\Admin\Git
 */
class GitController extends AbstractController
{
	/**
	 * @Route("/gitupdate", name="/gitupdate")
	 */
    public function updateAll():Response
    {
        $command = 'cd ' . ROOT_PATH;
        $command .= ' && git pull origin master';
        $res = [];
        exec($command, $res);
////        echo "updated\n";
//        var_dump($res);
	    return $this->render('/core/admin/git/git-info.html.twig', ['res' => $res, 'command' => $command]);
    }
}
