<?php

namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

/**
 * Обработка Ошибок
 * Class ErrorController
 * @package App\Controller
 */
class ErrorController extends AbstractController
{
	/**
	 * Показать ошибку
	 * @return Response
	 */
    public function show(): Response
    {
	    return $this->render('/bundles/TwigBundle/Exception/error404.html.twig');
    }
}
