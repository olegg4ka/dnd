<?php

namespace App\Controller\Auth;

use App\Application\Core\Codes\Codes;
use App\Application\Core\Forms\UserTypeForm;
use App\Entity\User;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AuthController extends AbstractController
{
	/**
	 * @Route("/auth", name="auth")
	 * @return Response
	 */
	public function authIn(Request $request): Response
	{

		$user = new User();
		$user->setLogin('');
		$user->setPassword('');
		$form = $this->createForm(UserTypeForm::class, $user);
		$form->handleRequest($request);
		if ($form->isSubmitted() && $form->isValid()) {
			$userInfo = $form->getData();
			$connection = $this->getDoctrine()->getRepository(User::class);
			$data = $connection->findOneBy(['login' => $userInfo->getLogin('login')]);
			if (!empty($data)){
				if ($data->getLogin('login') == $userInfo->getLogin('login') && $data->getPassword('password') == $userInfo->getPassword('password')){
					return $this->redirectToRoute('login');
				}else{
					return $this->redirectToRoute('failed');
				}
			}else{
				$entityManager = $this->getDoctrine()->getManager();
				$user = new User();
				$user->setLogin($userInfo->getLogin('login'));
				$user->setPassword( $userInfo->getPassword('password'));
				$entityManager->persist($user);
				$entityManager->flush();
				return new Response('Saved new product with id '.$user->getId());
			}
		}
		return $this->render('/app/auth/auth.html.twig', [
			'form' => $form->createView(),
		]);
	}

	/**
	 * @Route("/registr", name="registr")
	 */
	public function registration()
	{
		//TODO
	}

}