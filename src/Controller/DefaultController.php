<?php

namespace App\Controller;

use App\Application\Core\Menu\CreateMenu;
use App\Application\Core\MonologLogger\MonologLogger;
use App\Entity\Menu;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * Class DefaultController
 * @package App\Controller
 */
class DefaultController extends AbstractController
{
	/**
	 * @Route("/", name="/")
	 * @return Response
	 */
	public function index(): Response
	{
		$logger = new MonologLogger();
		$logger->db_log('test');
		$connection = $this->getDoctrine()->getRepository(Menu::class);
		return $this->render('/app/default/default.html.twig', [
				'menu' => (new CreateMenu())->renderMenu($connection->findAll())
			]
		);
	}
}