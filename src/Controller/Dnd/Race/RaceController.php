<?php

namespace App\Controller\Dnd\Race;

use App\Application\Core\Menu\CreateMenu;
use App\Application\Core\MonologLogger\MonologLogger;
use App\Application\Race\ViewRace;
use App\Entity\Menu;
use App\Entity\Race;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * Class DefaultController
 * @package App\Controller\Dnd\Race
 */
class RaceController extends AbstractController
{
	/**
	 * @Route("/race", name="race")
	 * @return Response
	 */
	public function race(): Response
	{
		$logger = new MonologLogger();
		$logger->db_log('test');
		$connMenu = $this->getDoctrine()->getRepository(Menu::class);
		$connRace = $this->getDoctrine()->getRepository(Race::class);
		return $this->render('/app/dnd/race/race.html.twig', [
				'menu' => (new CreateMenu())->renderMenu($connMenu->findAll()),
				'race' => (new ViewRace())->getInfoRace($connRace->findAll())
			]
		);
	}
}