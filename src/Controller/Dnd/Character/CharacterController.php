<?php

namespace App\Controller\Dnd\Character;

use App\Application\Core\Forms\CharacterForm;
use App\Application\Core\Menu\CreateMenu;
use App\Entity\CreateCharacter;
use App\Entity\Menu;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class CharacterController extends AbstractController
{
	/**
	 * @Route("/character", name="character")
	 * @return Response
	 */
	public function character(Request $request):Response
	{
		$form = $this->createForm(CharacterForm::class);
		$connection = $this->getDoctrine()->getRepository(Menu::class);
		if ($this->getUser()){
			$form->handleRequest($request);
			if ($form->isSubmitted() && $form->isValid()) {
				$dormData = $form->getData();
				$character = new CreateCharacter();
				$character->setNamecharacter($dormData->getNamecharacter());
				$character->setNameplayer($dormData->getNameplayer());
				$character->setClass($dormData->getClass());
				$character->setRace($dormData->getRace());
				$character->setUserid($this->getUser()->getId());
				$entityManager = $this->getDoctrine()->getManager();
				$entityManager->persist($character);
				$entityManager->flush();
				return $this->redirectToRoute('print');
			}
			return $this->render(
				'app/dnd/character.html.twig',
				array('form' => $form->createView(),'menu' => (new CreateMenu())->renderMenu($connection->findAll()))
			);
		}//todo else{} аторизуйтесь
		return $this->render(
			'app/dnd/character.html.twig',
			array('form' => $form->createView(),
				'menu' => (new CreateMenu())->renderMenu($connection->findAll())
			)
		);
	}
}