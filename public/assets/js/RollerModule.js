class RollerModule{

    checkValue(value){
        /**
         * Проверка есть ли в строке число
         */
        if (!isFinite(value) || value === "") {
            /**
             * Проверки на другие ошибки
             */
            switch (value) {
                /**
                 * Проверка на пустоту
                 * Проверка на undefined
                 * Проверка на null
                 *
                 */
                case null:
                case undefined:
                case '': {
                    alert('Введите значение');
                }
                    break;
                /**
                 * Проверка на все остальные возможные ошибки, например являетсья ли переменная строкой
                 */
                default: {
                    alert('Введите число');
                }
            }
        }else{
            //TODO
            return value;
        }
    }
}